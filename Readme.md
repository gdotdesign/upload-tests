# FORM / NORMAL POST
Only sends filename.

* ✔ query-string
* ✔ filename
* ✘ file in body
* ✘ file streamed
* ✘ Transfer-Encoding: chunked

Browser Compability:

* IE6+
* Chrome, Safari, Opera, Firefox

Request

    Query: { 'query-string': 'yes' }
    Body: { file: 'filename.ext' }
    Complete: true
    Transfer-Encoding: undefined

# FORM / MULTIPART POST
Browser loads the file fully then posts that.

* ✔ query-string
* ✔ filename
* ✔ file in body
* ✘ file streamed
* ✘ Transfer-Encoding: chunked

Browser Compability:

* IE6+
* Chrome, Safari, Opera, Firefox

Request

    Query: { 'query-string': 'yes' }
    Body: {}
    Files: filename.ext
    Complete: true
    Transfer-Encoding: undefined

# XMLHTTPREQUEST 2 UPLOAD
Streams file in request.

Cannot set `Transfer-Encoding` => `Refused to set unsafe header "Transfer-Encoding"`.

* ✔ query-string
* ✘ filename
* ✘ file in body
* ✔ file streamed
* ✘ Transfer-Encoding: chunked

Browser Compability:

* IE10+
* Chrome, Safari, Opera, Firefox

Request

    Query: { 'query-string': 'yes' }
    Body: {}
    Complete: false
    Transfer-Encoding: undefined

# XMLHTTPREQUEST POST
Streams file in request.

Cannot set `Transfer-Encoding` => `Refused to set unsafe header "Transfer-Encoding"`.

Loads files into memory.

* ✔ query-string
* ✘ filename
* ✘ file in body
* ✔ file streamed
* ✘ Transfer-Encoding: chunked

Browser Compability:

* IE10+
* Chrome, Safari, Opera, Firefox

Request

    Query: { 'query-string': 'yes' }
    Body: {}
    Complete: false
    Transfer-Encoding: undefined
