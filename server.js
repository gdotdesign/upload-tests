var express = require('express');
var app = express();
app.use(express.static('public'));
app.use(express.bodyParser());
app.post('/upload', function(req, res){
	console.log('Query:', req.query)
	console.log('Body:', req.body)
	if(req.files && req.files.file)
		console.log('Files:', req.files.file.name)
	console.log('Complete:', req.complete)
	console.log('Transfer-Encoding:', req.get('Transfer-Encoding'))
	req.on('data', function(){
		//console.log('Getting data....')
	})
	if(!req.complete)
		req.on('end',function(){res.send('done')})
	else
		res.send('done')
});

app.listen(3000);